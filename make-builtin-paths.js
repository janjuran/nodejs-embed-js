var fs = require("fs"),
	path = require("path");
const fse = require('fs-extra');
var argv = require('minimist')(process.argv.slice(2));

const strip = require('strip-comments');

console.log('This is a test module.');
console.log('Command line arguments: ', process.argv);
console.log('parsed arguments: ', argv);


console.log('path prefix: ', argv.prefix);

const prefix = argv.prefix? argv.prefix : '';
const dependenciesFolder = argv.dependencies? argv.dependencies : './node_modules';


var libraryFiles;
var additionalOutputLibraryFiles = new Array();
var solvedDependencies = new Map();	//key: is just the module name, value is the main file without js extension

//testing purposes only:
// fse.emptyDirSync('c:/devel/playground/node/make_builtin_paths/custom/');
// fse.copySync('c:/devel/playground/node_integrated_fastcall_backupfiles/custom_backup/', 'c:/devel/playground/node/make_builtin_paths/custom/');

if (argv.gypfile){
	console.log('baf');
	fs.readFile(argv.gypfile, 'utf8', function (err,data) {
		if (err) {
			return console.log(err);
		}
		
		let start = data.indexOf('library_files');
		let end = data.indexOf(']', start + 1);
		
		let files = data.substring(start - 1, end + 1);
		
		console.log(files);		
		files = files.replace(/'/g, '"');
		
		//remove comments:
		files = files.replace(/^\s*#.*/gm, '');
				
		//remove last comma if present:
		files = files.replace(/,\s*]/g, '\n]');
						
		libraryFiles = JSON.parse('{\n' + files + '\n}');
				
		fix_path('./custom/fastcall.js', undefined);

		console.log('[INFO]: Finished!');		
		additionalOutputLibraryFiles = additionalOutputLibraryFiles.map((str) => {
			return fixSlashes(path.join('lib', str + '.js'));
		});

		console.log('node-gyp library_files to add:');
		console.log(additionalOutputLibraryFiles);
	});
}
	
function walk(dir, callback) {
	fs.readdir(dir, function(err, files) {
		if (err) throw err;
		files.forEach(function(file) {			
			var filepath = path.join(dir, file);
			fs.stat(filepath, function(err,stats) {
				if (stats.isDirectory()) {
					walk(filepath, callback);
				} else if (stats.isFile()) {
					callback(filepath, stats);
				}
			});
		});
	});
}

const requirePattern = 'require(\'';
const requireRelative = 'require(\'.';

const requirePattern_double = 'require("';
const requireRelative_double = 'require(".';

function resolveSingleDouble(single, double){
	if (single == -1)
		return double;
	if (double == -1)
		return single;
	return Math.min(single, double);
}

function findNextRequire(data, pos){
	let single = data.indexOf(requirePattern, pos);
	let double = data.indexOf(requirePattern_double, pos);
	return resolveSingleDouble(single, double);
}

function findNextRelativeRequire(data, pos){	
	let single = data.indexOf(requireRelative, pos);
	let double = data.indexOf(requireRelative_double, pos);
	return resolveSingleDouble(single, double);
}

function findNextQuote(data, pos){
	let single = data.indexOf('\'', pos);
	let double = data.indexOf('"', pos);
	return resolveSingleDouble(single, double);
}

function findNextRelativePath(data, pos){
	let single = data.indexOf('\'.', pos);
	let double = data.indexOf('".', pos);
	return resolveSingleDouble(single, double);
}

function trimJSSuffix(filename){
	if (path.extname(filename) == '.js'){
		filename = filename.slice(0, -3);
		console.log('__TRIMEXT__', filename);
	}
	return filename;
}

function addJSSuffix(filename){
	if (path.extname(filename) != '.js'){
		//console.log('Adding js suffix...');
		filename += '.js';
	}
	return filename;
}

function getModuleName(data, pos){
	let start = findNextQuote(data, pos);
	let end = findNextQuote(data, start + 1);
	let name = data.substring(start + 1, end);
	name = trimJSSuffix(name);
	return {
		start : start,
		end :   end,
		name :  name
	}
}

function saveFile(filepath, data){
	fs.writeFile(filepath, data, 'utf8', function (err) {
		if (err) {
			console.log('[ERROR]: saving the file failed');
			return console.log(err);
		}
	});
}

function getIntegratedNodeModuleID(name){
	return 'lib/custom/node_modules/' + name;
}

function getDependencyPath(name){
	return 'custom/node_modules/' + name;
}


function isBaseModule(m){
	return (libraryFiles.library_files.indexOf('lib/' + m.name + '.js') != -1);
}

function fileAlreadyAdded(m){	
	return (additionalOutputLibraryFiles.indexOf(m.name + '.js') != -1);
}

function notYetAdded(m){	
	if (fileAlreadyAdded(m)) return false;	
	return !solvedDependencies.has(m.name);
}

function fixSlashes(path){
	return path.replace(/\\/g, '/');
}

var resolvedModules = new Map();

function resolveModulePath($moduleName, $context, $originalFileContext){	
	console.log('resolveModulePathNative\n\t $moduleName: %s, $context: %s, $originalFileContext: %s', $moduleName, $context, $originalFileContext);
	let cwdbackup = process.cwd();	

	$context = ($originalFileContext != undefined)? path.dirname($originalFileContext) : $context;

	if ($context != undefined){
		console.log('changing dir...');
		process.chdir($context);
		console.log(process.cwd());
	}
	console.log('Resolving module:\n\t$moduleName: %s, context: %s', $moduleName, $context);
	
	let nameToResolve = $moduleName;	
	if (fse.pathExistsSync(addJSSuffix(nameToResolve))){ /* this is a workaround for current dir js files that have the same name as any installed npm module */
		console.log('__RELATIVE__', nameToResolve);
		nameToResolve = addJSSuffix(nameToResolve);
	}
	else {
		let indexFile = './' + path.join(nameToResolve, 'index.js');		
		if (fse.pathExistsSync(indexFile)){ /* this is a workaround for require('./dir'), where dir is the local directory containing index.js file */			
			nameToResolve = fixSlashes(indexFile);			
			console.log('[INFO]: using local dir workaround: ', nameToResolve);
		}
	}
	let resolved;
	try{
		resolved = require.resolve(nameToResolve, { paths: [dependenciesFolder] });
	}
	catch(err){		
		console.log('[ERROR]: resolution of file %s failed ', nameToResolve);
		throw err;
	}
	finally{
		if ($context != undefined){
			process.chdir(cwdbackup);
		}
	}
	resolved = fixSlashes(resolved);
	console.log('Done, module resolved');	

	console.log('RESOLVED: ', resolved);	
	let result;
	if (resolvedModules.has(resolved)){
		result = resolvedModules.get(resolved);
	}
	else{
		let node_modules = resolved.lastIndexOf('node_modules');		
		if (node_modules != -1){
			let slash = resolved.indexOf('/', node_modules);
			let suffix = resolved.substr(slash + 1);
			console.log('SUFFIX_ ', suffix, node_modules, slash);
			fse.copySync(resolved, path.join('./custom/node_modules', suffix));
			result = fixSlashes(path.join('custom/node_modules', suffix));
			if (result.endsWith('.js')) result = result.slice(0,-3);
			console.log('Adding to resolved modules: ', result);
			resolvedModules.set(resolved, result);
		}
		else{
			console.log('Not a node module, looking in custom folder...');
			
			let filename = path.normalize(path.join($context, trimJSSuffix(nameToResolve)));
			filename = fixSlashes(path.relative('.', filename));
			
			let target = addJSSuffix(filename);
			if (!fse.pathExistsSync(target)){
				console.log('[INFO]: current custom file %s not yet handled. Copying now...', target);
				fse.copySync(resolved, target);
			}
			else {
				console.log('[INFO]: current custom file %s already exists. Skipping copying process...', target);
			}
			resolvedModules.set(resolved, result = filename);
		}
	}
	return { 
		resolvedSource : resolved,
		relativeTarget : result		
	};
}

function fix_path(filepath, stats, originalFileContext){
	if (path.basename(process.argv[1]) == filepath) {
		console.log('skipping self...');
		return;
	}

	//replace with forward slashes so as to avoid problems...
	filepath = filepath.replace(/\\/g, '/');
	
	console.log('fixing file: ', filepath);	
	var currentPrefix = path.dirname(filepath);
	console.log('current dir: ', currentPrefix);

	console.log('__FIX__', path.basename(filepath));

	let output = trimJSSuffix(filepath); //DELETE if not working...

	if (additionalOutputLibraryFiles.indexOf(output) == -1)
		additionalOutputLibraryFiles.push(output);
	else {
		console.log('[INFO]: this file (%s) has already been processed. Skipping...', filepath);
		return; /* because we have already went through this file */
	}
	
	try {
		let data = fs.readFileSync(addJSSuffix(filepath), 'utf8'); 		
			
		console.log('[INFO]: file read successfully -', filepath);

		console.log('Stripping comments...');
		data = strip(data);
		
		var pos = 0;

		while ((pos = findNextRequire(data, pos)) !== -1){			
			let m = getModuleName(data, pos);
			console.log('(file, require): (%s, %s)', filepath, m.name);

			if (!isBaseModule(m)){				
				let resolved = resolveModulePath(m.name, currentPrefix, originalFileContext);
						
				data = data.slice(0, m.start + 1) + resolved.relativeTarget + data.slice(m.end);
				pos = m.start + resolved.relativeTarget.length;
			
				console.log('calling fix_path for file:', resolved.relativeTarget);				
				fix_path(resolved.relativeTarget, stats, resolved.resolvedSource);
			}
			else {
				console.log('[INFO]: this is a native module. Skipping...');
				pos = m.end;
			}		
		}
		console.log('SAVE saving to:', addJSSuffix(filepath));
		saveFile(addJSSuffix(filepath), data);
	}
	catch (err){		
		//console.log('[ERROR]: file couldnt be read: ', filepath);
		console.log('[ERROR]: exception while parsing file: ', filepath);
		return console.log(err);		
	}
}
